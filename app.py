import web
import xml.etree.ElementTree as ET
import lxml.etree as etree

# Loading file
tree = ET.parse('temperature.xml')
error_tree = etree.parse("temperatureError.xml")
root = tree.getroot()

# Generating routes
urls = (
    '/c/(.*)', 'celsius',
    '/f/(.*)', 'fahrenheit',
    '/k/(.*)', 'kelvin',
)
app = web.application(urls, globals())


class celsius:
    def GET(self, celsius_input) -> str:
        try:
            celsius_input = float(celsius_input)
        except ValueError:
            return etree.tostring(error_tree)

        root.find('celsius').text = str(celsius_input)
        root.find('fahrenheit').text = str(9.0 / 5.0 * celsius_input + 32)
        root.find('kelvin').text = str(celsius_input + 273.15)

        tree.write("temperature.xml")
        test_tree = etree.parse("temperature.xml")
        return etree.tostring(test_tree)


class fahrenheit:
    def GET(self, fahrenheit_input) -> str:
        try:
            fahrenheit_input = float(fahrenheit_input)
        except ValueError:
            return etree.tostring(error_tree)

        root.find('celsius').text = str((fahrenheit_input - 32) * 5.0 / 9.0)
        root.find('fahrenheit').text = str(fahrenheit_input)
        root.find('kelvin').text = str(5 * (fahrenheit_input - 32) / 9 + 273.15)

        tree.write("temperature.xml")
        test_tree = etree.parse("temperature.xml")
        return etree.tostring(test_tree)


class kelvin:
    def GET(self, kelvin_input) -> str:
        try:
            kelvin_input = float(kelvin_input)
        except ValueError:
            return etree.tostring(error_tree)

        root.find('celsius').text = str(kelvin_input - 273.15)
        root.find('fahrenheit').text = str((kelvin_input - 273.15) * 1.8 + 32)
        root.find('kelvin').text = str(kelvin_input)

        tree.write("temperature.xml")
        test_tree = etree.parse("temperature.xml")
        return etree.tostring(test_tree)


if __name__ == "__main__":
    app.run()
